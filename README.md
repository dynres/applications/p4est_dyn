# Dynamic Resource Extensions for P4est
This repository contains dynamic resource extensions for the p4est library 

## Prerequisites
- [P4est](https://www.p4est.org/)
- [Open-MPI with DPP](https://gitlab.inria.fr/dynres/dyn-procs/ompi)

## Installation
The library can be installed with autotools. In this directory:

1. Run autogen
```
./autogen.sh
```
2. Run configure (optionally specify an install prefix and the paths to p4est and mpi if they are not installed in a standard location)
```
./configure [--prefix=/path/to/install-dir] [--with-p4est=/path/to/p4est] [--with-mpi=/path/to/mpi]
```
3. Run make
```
make && make install
```