#!/bin/sh

echo "*******************************************************"
echo "Let's build the dynamic resource extensions for P4est!"
echo "*******************************************************"
echo ""

autoreconf -i

echo "autogen: SUCCESS! You can now run ./configure [--prefix=/path/to/install-dir] [--with-p4est=/path/to/p4est] [--with-mpi=/path/to/mpi]"