#include "p4est_dynres.h"
#include "p4est_dynres_internal.h"
#include "p4est_communication.h"

void send_setop(p4est_dynres_state_t * state){
    char ** input_psets;

    p4est_dynres_data_t * data = (p4est_dynres_data_t *) state->p4est_dynres_data;

                                                                              
    input_psets = (char **)malloc(1 * sizeof(char *));                                                                                 
    input_psets[0] = strdup(data->main_pset);                                                                                                
    data->setop = MPI_PSETOP_REPLACE;                                                                                                    
    MPI_Session_dyn_v2a_psetop_nb(data->session, &data->setop, input_psets, 1, &data->output_psets, &data->noutput_psets, data->col, &data->request);                   

    data->setop_pending = true;
}

void check_setop(p4est_dynres_state_t * state, int * flag){
    MPI_Status status;
    MPI_Info info;
    p4est_dynres_data_t * data = (p4est_dynres_data_t *) state->p4est_dynres_data;


    MPI_Test(&data->request, flag, &status);
    if(*flag){
        if(data->setop == MPI_PSETOP_REPLACE){
            data->request = MPI_REQUEST_NULL;
            strcpy(data->delta_sub, data->output_psets[0]);
            strcpy(data->delta_add, data->output_psets[1]);
            strcpy(data->main_pset, data->output_psets[2]);
            free_string_array(&data->output_psets, data->noutput_psets);

            MPI_Info_create(&info);                                                                             
            MPI_Info_set(info, "p4est_pset", data->main_pset);                                                  
            MPI_Session_set_pset_data(data->session, data->delta_add, info);
            MPI_Info_free(&info);  
            
            data->noutput_psets = 0;
        }else{
            /* Try again */
            send_setop(state);
            *flag = false;
        }
    }
}

void handle_setop(p4est_dynres_state_t * state, int * terminate, int *reconfigured){

    int flag;
    char boolean_string[10];
    MPI_Group group;
    MPI_Comm comm;
    MPI_Info info;

    p4est_dynres_data_t * data = (p4est_dynres_data_t *) state->p4est_dynres_data;

    *reconfigured = 1;
    data->setop_pending = false;

    /* Distribute the new pset names */
    sc_MPI_Bcast(data->delta_add, MPI_MAX_PSET_NAME_LEN, sc_MPI_CHAR, 0, state->p4est->mpicomm);
    sc_MPI_Bcast(data->delta_sub, MPI_MAX_PSET_NAME_LEN, sc_MPI_CHAR, 0, state->p4est->mpicomm);
    sc_MPI_Bcast(data->main_pset, MPI_MAX_PSET_NAME_LEN, sc_MPI_CHAR, 0, state->p4est->mpicomm);

    /* Check if this process is included in the new PSet */                                        
    MPI_Session_get_pset_info(data->session, data->main_pset, &info);                                                                            
    MPI_Info_get(info, "mpi_included", 6, boolean_string, &flag);                                                                      
    MPI_Info_free(&info);

    if(0 != strcmp(boolean_string, "True")){

        /* Send away our p4est data before we leave */
        state->p4est = p4est_dynres_replace_ext(state->p4est, sc_MPI_COMM_NULL,
                        -1, -1, -1, -1, (void *) state->p4est->user_pointer, NULL);
        *terminate = 1;
        return;
    }

    /* Create a new comm */
    MPI_Group_from_session_pset (data->session, data->main_pset, &group);
    MPI_Comm_create_from_group(group, data->main_pset, MPI_INFO_NULL, MPI_ERRORS_RETURN, &comm);
    MPI_Group_free(&group);

    /* Replace communicator of p4est */
    state->p4est = p4est_dynres_replace_ext(state->p4est, (sc_MPI_Comm) comm,
                        -1, -1, -1, -1, (void *) state->p4est->user_pointer, NULL);
    state->p4est->mpicomm_owned = 1;

    if(0 != strcmp("",data->delta_add)){
        /* Pack and send the user data */
        data->pack(state);
        p4est_dynres_send_info(state->p4est->mpicomm, data->info);
    }

    /* Rank 0 of new comm finalizes the set operation*/
    if(state->p4est->mpirank == 0){
        MPI_Session_dyn_finalize_psetop(data->session, data->main_pset);
    }
}

p4est_dynres_state_t * p4est_dynres_init(const char *initial_pset, p4est_connectivity_t * connectivity,
           size_t data_size, p4est_init_t init_fn, void *user_pointer,
           p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack, MPI_Info col){

    MPI_Comm comm;
    MPI_Group group;
    MPI_Info pset_info;
    char pset_alias[MPI_MAX_PSET_NAME_LEN];
    int flag = 0;

    p4est_dynres_state_t *p4est_state = p4est_dynres_state_new(initial_pset, pack, unpack, user_pointer);
    if(NULL == p4est_state){
        return NULL;
    }

    p4est_dynres_data_t * dynres_data = (p4est_dynres_data_t *) p4est_state->p4est_dynres_data;

    if(MPI_INFO_NULL != col){
        MPI_Info_dup(col, &dynres_data->col); 
    }

    /* Get the PSet alias to ensure we have a unique tag for communicator creation */
    MPI_Session_get_pset_info (dynres_data->session, dynres_data->main_pset, &pset_info);
    MPI_Info_get(pset_info, "mpi_alias", MPI_MAX_PSET_NAME_LEN, pset_alias, &flag);
    if(!flag){
        p4est_dynres_state_free(&p4est_state);
        return NULL;
    }

    /* create a group from pset */
    MPI_Group_from_session_pset (dynres_data->session, dynres_data->main_pset, &group);

    /* create a communicator from group */
    MPI_Comm_create_from_group(group, pset_alias, MPI_INFO_NULL, MPI_ERRORS_RETURN, &comm);
    MPI_Group_free(&group);

    if(p4est_state->is_dynamic){   
        /* Replace communicator of p4est */
        p4est_state->p4est = p4est_dynres_replace_ext(p4est_state->p4est, (sc_MPI_Comm) comm,
                        -1, -1, -1, -1, (void *) user_pointer, NULL);
        p4est_state->p4est->mpicomm_owned = 1;
        /* Recv data */
        p4est_dynres_recv_info(p4est_state->p4est->mpicomm, &dynres_data->info);
        /* unpack data */
        dynres_data->unpack(p4est_state);
    }else{
        /* create the p4est */
        p4est_state->p4est = p4est_new((sc_MPI_Comm) comm, connectivity, data_size, init_fn, user_pointer);
        p4est_state->p4est->mpicomm_owned = 1;
    }

    /* Store our rank */
    dynres_data->rank = p4est_state->p4est->mpirank;

    /* If this was the launch of the original processes we send a set operation right away  
     * If this was a dynamic launch we might need to finalize the set operation
     */
    if(!p4est_state->is_dynamic){ 
        if(dynres_data->col != MPI_INFO_NULL){

            dynres_data->setop_pending = true;

            if(p4est_state->p4est->mpirank == 0){
                send_setop(p4est_state);
            }
        }else{
            dynres_data->setop_pending = false;
        }
    }else{
        dynres_data->setop_pending = true;

        if(p4est_state->p4est->mpirank == 0){
            MPI_Session_dyn_finalize_psetop(dynres_data->session, dynres_data->main_pset);
        }        
    }

    return p4est_state;

}

int p4est_dynres_adapt(p4est_dynres_state_t * state, int *terminate, int *reconfigured){

    int flag;

    p4est_dynres_data_t *dynres_data =  (p4est_dynres_data_t *) state->p4est_dynres_data;

    *terminate = 0;
    *reconfigured = 0;
    
    /* If we have a pending setop we need to check if it was executed 
     * and if so adapt accordingly
     */
    if(dynres_data->setop_pending){
        /* Rank 0 requested the setop, so it does the check and Bcasts the result */
        if(state->p4est->mpirank == 0){
            check_setop(state, &flag);
        }
        
        sc_MPI_Bcast(&flag, 1, sc_MPI_INT, 0, state->p4est->mpicomm);

        /* Setop still pending, so bail out here */
        if(!flag){
            return 0;
        }

        handle_setop(state, terminate, reconfigured);
        if(*terminate){
            dynres_data->setop_pending = false;
            return 0;
        }

        /* store our new rank */
        dynres_data->rank = state->p4est->mpirank;
    }

    if(!dynres_data->setop_pending){                                                                                                                                
        if (state->p4est->mpirank == 0)                                                                                    
        {                                                                                                                                      
            send_setop(state);                                                                                                         
        }

        dynres_data->setop_pending = true;
    } 

    return 0;

}                                                                                                                                      
     
int p4est_dynres_finalize(p4est_dynres_state_t ** state, char *final_pset){

    char **input_psets, **output_psets;
    char cancel_value[MPI_MAX_PSET_NAME_LEN];
    int flag, setop, noutput = 0;
    MPI_Info info;

    char sub_pset[MPI_MAX_PSET_NAME_LEN];
    char cancel_pset[MPI_MAX_PSET_NAME_LEN] = ""; 
    char boolean_string[16] = "";

    p4est_dynres_state_t *p4est_state = *state;
    p4est_dynres_data_t *data = p4est_state->p4est_dynres_data;
    p4est_connectivity_t *connectivity;


    /* If we have a pending setop, primary needs to cancel it and send result to all procs */
    strcpy(sub_pset, data->main_pset);
    if (data->setop_pending){
        if(data->rank == 0){

            /* Send the setop cancelation - op will be MPI_PSETOP_NULL if setop already applied */
            setop = MPI_PSETOP_CANCEL;                                                                                     
            input_psets = (char **)malloc(1 * sizeof(char *));                                                              
            input_psets[0] = strdup(data->main_pset);                                                                             
            noutput = 0;
            MPI_Session_dyn_v2a_psetop(data->session, &setop, input_psets, 1, &output_psets, &noutput, MPI_INFO_NULL);       
            free_string_array(&input_psets, 1);
            free_string_array(&output_psets, noutput);

            /* If cancelation did not succeed finalize outstanding setop and store pset names */
            if (MPI_PSETOP_NULL == setop)                                                                                  
            {
                MPI_Wait(&data->request, MPI_STATUS_IGNORE);
                if (MPI_PSETOP_NULL != data->setop)                                                                            
                {                                                                                                   
                    strcpy(cancel_pset, data->output_psets[1]);
                    strcpy(sub_pset, data->output_psets[2]);                                                                 
                    MPI_Session_dyn_finalize_psetop(data->session, data->main_pset);          
                    free_string_array(&data->output_psets, data->noutput_psets);                                            
                }                                                                       
            }
        }       
    }

    /* Destroy the P4est (this is synchronizing)*/
    /* P4est is already NULL for procs that are dynamically removed */
    if(p4est_state->p4est != NULL) {
        //connectivity = p4est_state->p4est->connectivity;
        sc_MPI_Bcast(sub_pset, MPI_MAX_PSET_NAME_LEN, sc_MPI_CHAR, 0, p4est_state->p4est->mpicomm);
        /* Check if this process is included in the final sub PSet */                                        
        MPI_Session_get_pset_info(data->session, sub_pset, &info);                                                                            
        MPI_Info_get(info, "mpi_included", 6, boolean_string, &flag);                                                                      
        MPI_Info_free(&info);
        p4est_dynres_destroy(p4est_state->p4est);
        //if(NULL != connectivity){
        //    p4est_connectivity_destroy(connectivity);
        //}
    }

    if(data->rank == 0){
        /* Send a SUB operation to delete this PSet and inform spawned processes about cancelation */
        setop = MPI_PSETOP_SUB;
        input_psets = (char **)malloc(1 * sizeof(char *));                                                              
        input_psets[0] = strdup(sub_pset);
        noutput = 0;
        MPI_Info_create(&info);
        MPI_Info_set(info, "model", "DefaultSubModel()");
        MPI_Info_set(info, "output_space_generator", "output_space_generator_sub");
        MPI_Info_set(info, "input_pset_models_0", "NullPSetModel()");   

        MPI_Session_dyn_v2a_psetop(data->session, &setop, input_psets, 1, &output_psets, &noutput, info);
        MPI_Info_free(&info);
        free_string_array(&input_psets, 1);
        free_string_array(&output_psets, noutput);

         /* If we have a cancel PSet, inform spawned processes about cancelation */
        if(0 != strcmp(cancel_pset, "")){
            sprintf(cancel_value, "p4est://canceled/%s", sub_pset);
            /* Inform spawned processes about cancelation */
            MPI_Info_create(&info);                                                                             
            MPI_Info_set(info, "p4est_pset", cancel_value);                                                  
            MPI_Session_set_pset_data(data->session, cancel_pset, info);
            MPI_Info_free(&info);
        }       
    }

    /* If we are part of the final sub operation Wait for primary proc */
    if(0 == strcmp(boolean_string, "True")){ 
        input_psets = malloc(sizeof(char *));
        input_psets[0] = strdup(sub_pset);
        MPI_Session_pset_barrier(data->session, input_psets, 1, MPI_INFO_NULL);
        free_string_array(&input_psets, 1);
    }

    p4est_dynres_state_free(state);

    if(NULL != final_pset){
        strcpy(final_pset, sub_pset);
    }

    return 0;
}

int p4est_dynres_pack_string(p4est_dynres_state_t * state, const char * key, char *val){
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        MPI_Info_create(&data->info);
    }
    MPI_Info_set(data->info, key, val);
    return 0;
}
int p4est_dynres_pack_bool(p4est_dynres_state_t * state, const char * key, bool val){
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        MPI_Info_create(&data->info);
    }

    sprintf(string, "%s", val ? "True" : "False");
    MPI_Info_set(data->info, key, string);
    return 0;
}
int p4est_dynres_pack_int(p4est_dynres_state_t * state, const char * key, int val){
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        MPI_Info_create(&data->info);
    }
    sprintf(string, "%d", val);
    MPI_Info_set(data->info, key, string);
    return 0;
}
int p4est_dynres_pack_float(p4est_dynres_state_t * state, const char * key, float val){
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        MPI_Info_create(&data->info);
    }

    sprintf(string, "%f", val);
    MPI_Info_set(data->info, key, string);
    return 0;
}
int p4est_dynres_pack_double(p4est_dynres_state_t * state, const char * key, double val){
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        MPI_Info_create(&data->info);
    }

    sprintf(string, "%lf", val);
    MPI_Info_set(data->info, key, string);
    return 0;
}

int p4est_dynres_unpack_string(p4est_dynres_state_t * state, const char * key, char **val){
    int flag = 0;
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        return -1;
    }
    MPI_Info_get(data->info, key, MPI_MAX_INFO_VAL, string, &flag);
    if(!flag){
        return -1;
    }
    *val = strdup(string);
    return 0;
}
int p4est_dynres_unpack_bool(p4est_dynres_state_t * state, const char * key, bool *val){
    int flag = 0;
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        return -1;
    }
    MPI_Info_get(data->info, key, MPI_MAX_INFO_VAL, string, &flag);
    if(!flag){
        return -1;
    }
    *val = (0 == strcmp("True", string)) ? true : false;
    return 0;
}
int p4est_dynres_unpack_int(p4est_dynres_state_t * state, const char * key, int *val){
    int flag = 0;
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        return -1;
    }
    MPI_Info_get(data->info, key, MPI_MAX_INFO_VAL, string, &flag);
    if(!flag){
        return -1;
    }
    *val = atoi(string);
    return 0;    
}
int p4est_dynres_unpack_float(p4est_dynres_state_t * state, const char * key, float *val){
    int flag = 0;
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        return -1;
    }
    MPI_Info_get(data->info, key, MPI_MAX_INFO_VAL, string, &flag);
    if(!flag){
        return -1;
    }
    *val = atof(string);
    return 0;
}
int p4est_dynres_unpack_double(p4est_dynres_state_t * state, const char * key, double *val){
    int flag = 0;
    char string[MPI_MAX_INFO_VAL];
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) state->p4est_dynres_data;
    if(data->info == MPI_INFO_NULL){
        return -1;
    }
    MPI_Info_get(data->info, key, MPI_MAX_INFO_VAL, string, &flag);
    if(!flag){
        return -1;
    }

    *val = strtod(string, NULL);

    return 0;
}

void p4est_dynres_set_info(p4est_dynres_state_t *state, MPI_Info info){
    p4est_dynres_data_t *data = state->p4est_dynres_data;
    if(MPI_INFO_NULL != data->col){
        MPI_Info_free(&data->col);
    }

    if(MPI_INFO_NULL != info){
        MPI_Info_dup(info, &data->col);
    }
}