#ifndef P4EST_DYNRES_H
#define P4EST_DYNRES_H

#include "mpi.h"
#include "p4est.h"
#include "p4est_dynres_common.h"
#include <stdbool.h>


int p4est_dynres_pack_string(p4est_dynres_state_t * state, const char * key, char *val);
int p4est_dynres_pack_bool(p4est_dynres_state_t * state, const char * key, bool val);
int p4est_dynres_pack_int(p4est_dynres_state_t * state, const char * key, int val);
int p4est_dynres_pack_float(p4est_dynres_state_t * state, const char * key, float val);
int p4est_dynres_pack_double(p4est_dynres_state_t * state, const char * key, double val);

int p4est_dynres_unpack_string(p4est_dynres_state_t * state, const char * key, char **val);
int p4est_dynres_unpack_bool(p4est_dynres_state_t * state, const char * key, bool *val);
int p4est_dynres_unpack_int(p4est_dynres_state_t * state, const char * key, int *val);
int p4est_dynres_unpack_float(p4est_dynres_state_t * state, const char * key, float *val);
int p4est_dynres_unpack_double(p4est_dynres_state_t * state, const char * key, double *val);

p4est_dynres_state_t * p4est_dynres_init(const char *initial_pset, p4est_connectivity_t * connectivity,
           size_t data_size, p4est_init_t init_fn, void *user_pointer,
           p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack, MPI_Info col);
int p4est_dynres_adapt(p4est_dynres_state_t * state, int *terminate, int *reconfigured);
int p4est_dynres_finalize(p4est_dynres_state_t ** state, char *final_pset);

void p4est_dynres_set_info(p4est_dynres_state_t *state, MPI_Info info);

#endif /* !P4EST_DYNRES_H */
