#ifndef P4EST_DYNRES_COMMON_H
#define P4EST_DYNRES_COMMON_H

#include "p4est.h"
#include "p4est_connectivity.h"
#include "stdbool.h"

typedef struct{
    p4est_t *p4est;
    void * p4est_dynres_data;
    void *user_pointer;
    bool is_dynamic;
} p4est_dynres_state_t;

typedef int (*p4est_dynres_pack_func_t) (p4est_dynres_state_t * state);
typedef int (*p4est_dynres_unpack_func_t) (p4est_dynres_state_t * state);
#endif /* !P4EST_DYNRES_COMMON_H */