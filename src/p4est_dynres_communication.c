/*
  This file is part of p4est.
  p4est is a C library to manage a collection (a forest) of multiple
  connected adaptive quadtrees or octrees in parallel.

  Copyright (C) 2010 The University of Texas System
  Additional copyright (C) 2011 individual authors
  Written by Carsten Burstedde, Lucas C. Wilcox, and Tobin Isaac

  p4est is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  p4est is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with p4est; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifdef P4_TO_P8
#include <p8est_algorithms.h>
#include <p8est_communication.h>
#include <p8est_bits.h>
#else
#include <p4est_algorithms.h>
#include <p4est_communication.h>
#include "p4est_dynres_communication.h"
#include <p4est_bits.h>
#endif /* !P4_TO_P8 */
#include <sc_search.h>
#ifdef P4EST_HAVE_ZLIB
#include <zlib.h>
#endif

void
p4est_dynres_destroy (p4est_t * p4est)
{
#ifdef P4EST_ENABLE_DEBUG
  size_t              qz;
#endif
  p4est_topidx_t      jt;
  p4est_tree_t       *tree;

  for (jt = 0; jt < p4est->connectivity->num_trees; ++jt) {
    tree = p4est_tree_array_index (p4est->trees, jt);

#ifdef P4EST_ENABLE_DEBUG
    for (qz = 0; qz < tree->quadrants.elem_count; ++qz) {
      p4est_quadrant_t   *quad =
        p4est_quadrant_array_index (&tree->quadrants, qz);
      p4est_quadrant_free_data (p4est, quad);
    }
#endif

    sc_array_reset (&tree->quadrants);
  }
  sc_array_destroy (p4est->trees);

  if (p4est->user_data_pool != NULL) {
    sc_mempool_destroy (p4est->user_data_pool);
  }
  sc_mempool_destroy (p4est->quadrant_pool);

  p4est_dynres_comm_parallel_env_release (p4est);
  P4EST_FREE (p4est->global_first_quadrant);
  P4EST_FREE (p4est->global_first_position);
  P4EST_FREE (p4est);
}

void
p4est_dynres_comm_parallel_env_release (p4est_t * p4est)
{
  int                 mpiret;

  /* free MPI communicator if it's owned */
  if (p4est->mpicomm_owned) {
    mpiret = MPI_Comm_disconnect (&(p4est->mpicomm));
    SC_CHECK_MPI (mpiret);
  }
  p4est->mpicomm = MPI_COMM_NULL;
  p4est->mpicomm_owned = 0;

  /* set MPI information */
  p4est->mpisize = 0;
  p4est->mpirank = MPI_UNDEFINED;
}

void
p4est_dynres_comm_parallel_env_replace (p4est_t * p4est, MPI_Comm mpicomm)
{
  /* check if input MPI communicator has same size and same rank order */
#ifdef P4EST_ENABLE_DEBUG
  {
    int                 mpiret, result;

    mpiret = MPI_Comm_compare (p4est->mpicomm, mpicomm, &result);
    SC_CHECK_MPI (mpiret);

    //Assertion removed, as it prevents resource changes
    //P4EST_ASSERT (result == MPI_IDENT || result == MPI_CONGRUENT);
  }
#endif

  /* release the current parallel environment */
  p4est_dynres_comm_parallel_env_release (p4est);

  /* assign new MPI communicator */
  p4est_comm_parallel_env_assign (p4est, mpicomm);
}

int
p4est_dynres_comm_parallel_env_reduce (p4est_t ** p4est_supercomm)
{
  return p4est_dynres_comm_parallel_env_reduce_ext (p4est_supercomm,
                                             MPI_GROUP_NULL, 0, NULL);
}

int
p4est_dynres_comm_parallel_env_reduce_ext (p4est_t ** p4est_supercomm,
                                    MPI_Group group_add,
                                    int add_to_beginning, int **ranks_subcomm)
{
  const char         *this_fn_name = "comm_parallel_env_reduce";
  p4est_t            *p4est = *p4est_supercomm;
  MPI_Comm         mpicomm = p4est->mpicomm;
  int                 mpisize = p4est->mpisize;
  int                 mpiret;
  p4est_gloidx_t     *global_first_quadrant = p4est->global_first_quadrant;
  p4est_quadrant_t   *global_first_position = p4est->global_first_position;

  p4est_gloidx_t     *n_quadrants;
  int                *include;
  MPI_Group        group, subgroup;
  MPI_Comm         submpicomm;
  int                 submpisize, submpirank;
  int                *ranks, *subranks;
  int                 p;

  /* exit if MPI communicator cannot be reduced */
  if (mpisize == 1) {
    return 1;
  }

  /* create array of non-empty processes that will be included to sub-comm */
  n_quadrants = P4EST_ALLOC (p4est_gloidx_t, mpisize);
  include = P4EST_ALLOC (int, mpisize);
  submpisize = 0;
  for (p = 0; p < mpisize; p++) {
    n_quadrants[p] = global_first_quadrant[p + 1] - global_first_quadrant[p];
    if (global_first_quadrant[p] < global_first_quadrant[p + 1]) {
      include[submpisize++] = p;
    }
  }

  /* exit if reduction not possible */
  if (submpisize == mpisize) {
    P4EST_FREE (n_quadrants);
    P4EST_FREE (include);
    return 1;
  }

  /* create sub-group of non-empty processors */
  mpiret = MPI_Comm_group (mpicomm, &group);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Group_incl (group, submpisize, include, &subgroup);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Group_free (&group);
  SC_CHECK_MPI (mpiret);
  P4EST_FREE (include);

  /* create sub-communicator */
  if (group_add != MPI_GROUP_NULL) {
    MPI_Group        group_union;

    /* create union with optional group */
    if (add_to_beginning) {
      mpiret = MPI_Group_union (group_add, subgroup, &group_union);
    }
    else {
      mpiret = MPI_Group_union (subgroup, group_add, &group_union);
    }
    SC_CHECK_MPI (mpiret);

    /* create sub-communicator */
    mpiret = MPI_Comm_create (mpicomm, group_union, &submpicomm);
    SC_CHECK_MPI (mpiret);
    mpiret = MPI_Group_free (&group_union);
    SC_CHECK_MPI (mpiret);
    mpiret = MPI_Group_free (&subgroup);
    SC_CHECK_MPI (mpiret);
  }
  else {
    /* create sub-communicator */
    mpiret = MPI_Comm_create (mpicomm, subgroup, &submpicomm);
    SC_CHECK_MPI (mpiret);
    mpiret = MPI_Group_free (&subgroup);
    SC_CHECK_MPI (mpiret);
  }
  /* destroy p4est and exit if this rank is empty */
  if (submpicomm == MPI_COMM_NULL) {
    /* destroy */
    P4EST_FREE (n_quadrants);
    p4est_dynres_destroy (p4est);
    *p4est_supercomm = NULL;
    if (ranks_subcomm) {
      *ranks_subcomm = NULL;
    }
    /* return that p4est does not exist on this rank */
    return 0;
  }

  /* update parallel environment */
  mpiret = MPI_Comm_size (submpicomm, &submpisize);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_rank (submpicomm, &submpirank);
  SC_CHECK_MPI (mpiret);

  if (submpirank == 0) {
    P4EST_VERBOSEF ("%s: Reduce MPI communicator from %i to %i\n",
                    this_fn_name, mpisize, submpisize);
    /* TODO: There should be a function for printing to stdout that works with
     *       sub-communicators. */
  }

  /* translate MPI ranks */
  ranks = P4EST_ALLOC (int, submpisize);
  subranks = P4EST_ALLOC (int, submpisize);
  for (p = 0; p < submpisize; p++) {
    subranks[p] = p;
  }
  mpiret = MPI_Comm_group (submpicomm, &subgroup);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Comm_group (mpicomm, &group);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Group_translate_ranks (subgroup, submpisize, subranks,
                                         group, ranks);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Group_free (&subgroup);
  SC_CHECK_MPI (mpiret);
  mpiret = MPI_Group_free (&group);
  SC_CHECK_MPI (mpiret);
  P4EST_FREE (subranks);

  /* allocate and set global quadrant count */
  P4EST_FREE (p4est->global_first_quadrant);
  p4est->global_first_quadrant = P4EST_ALLOC (p4est_gloidx_t, submpisize + 1);
  p4est->global_first_quadrant[0] = 0;
  for (p = 0; p < submpisize; p++) {
    P4EST_ASSERT (ranks[p] != MPI_UNDEFINED);
    P4EST_ASSERT (group_add != MPI_GROUP_NULL
                  || 0 < n_quadrants[ranks[p]]);
    p4est->global_first_quadrant[p + 1] =
      p4est->global_first_quadrant[p] + n_quadrants[ranks[p]];
  }
  P4EST_ASSERT (p4est->global_first_quadrant[submpisize] =
                p4est->global_num_quadrants);
  P4EST_FREE (n_quadrants);

  /* set new parallel environment */
  p4est_dynres_comm_parallel_env_release (p4est);
  p4est_comm_parallel_env_assign (p4est, submpicomm);
  //p4est_comm_parallel_env_duplicate (p4est);
  //mpiret = MPI_Comm_disconnect (&submpicomm);
  //SC_CHECK_MPI (mpiret);
  P4EST_ASSERT (p4est->mpisize == submpisize);

  /* allocate and set partition information */
  p4est->global_first_position =
    P4EST_ALLOC (p4est_quadrant_t, submpisize + 1);
  if (group_add != MPI_GROUP_NULL) { /* if communication is required */
    p4est_comm_global_partition (p4est, NULL);
  }
  else {                        /* if we can set partition information communication-free */
    for (p = 0; p < submpisize; p++) {
      P4EST_ASSERT (0 == p || ranks[p - 1] < ranks[p]);
      p4est->global_first_position[p] = global_first_position[ranks[p]];
    }
    p4est->global_first_position[submpisize] = global_first_position[mpisize];
  }
  P4EST_FREE (global_first_position);
  if (ranks_subcomm) {
    *ranks_subcomm = ranks;
  }
  else {
    P4EST_FREE (ranks);
  }

  /* check for valid p4est */
  P4EST_ASSERT (p4est_is_valid (p4est));

  /* return that p4est exists on this rank */
  return 1;
}

