/*
  This file is part of p4est.
  p4est is a C library to manage a collection (a forest) of multiple
  connected adaptive quadtrees or octrees in parallel.

  Copyright (C) 2010 The University of Texas System
  Additional copyright (C) 2011 individual authors
  Written by Carsten Burstedde, Lucas C. Wilcox, and Tobin Isaac

  p4est is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  p4est is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with p4est; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef P4EST_DYNRES_COMMUNICATION_H
#define P4EST_DYNRES_COMMUNICATION_H

#include <p4est.h>

SC_EXTERN_C_BEGIN;

void
p4est_dynres_destroy (p4est_t * p4est);

/** Release MPI communicator if it is owned by p4est.
 */
void                p4est_dynres_comm_parallel_env_release (p4est_t * p4est);

/** Replace the current MPI communicator by the one provided as input.
 *
 * \param [in] mpicomm    A valid MPI communicator.
 *
 * \note The provided MPI communicator is not owned by p4est.
 */
void                p4est_dynres_comm_parallel_env_replace (p4est_t * p4est,
                                                     sc_MPI_Comm mpicomm);


int                 p4est_dynres_comm_parallel_env_reduce (p4est_t **
                                                    p4est_supercomm);

/** Reduce MPI communicator to non-empty ranks and add a group of ranks that
 * will remain in the reduced communicator regardless whether they are empty
 * or not.
 *
 * \param [in/out] p4est_supercomm  Object which communicator is reduced.
 *                                  Points to NULL if this p4est does not
 *                                  exists.
 * \param [in] group_add         Group of ranks that will remain in
 *                               communicator.
 * \param [in] add_to_beginning  If true, ranks will be added to the beginning
 *                               of the reduced communicator, otherwise to the
 *                               end.
 * \param[out] ranks_subcomm     If not null, array of size 'subcommsize' with
 *                               subcommrank->supercommrank map.
 *
 * \return True if p4est exists on this MPI rank after reduction.
 */
int                 p4est_dynres_comm_parallel_env_reduce_ext (p4est_t **
                                                        p4est_supercomm,
                                                        sc_MPI_Group
                                                        group_add,
                                                        int add_to_beginning,
                                                        int **ranks_subcomm);
SC_EXTERN_C_END;

#endif /* !P4EST_DYNRES_COMMUNICATION_H */