#include "mpi.h"
#include "p4est.h"
#include "p4est_communication.h"
#include "p4est_algorithms.h"
#include "p4est_dynres_internal.h"
#include <stdlib.h>
#include <stddef.h>

// https://stackoverflow.com/questions/40807833/sending-size-t-type-data-with-mpi
#if SIZE_MAX == UCHAR_MAX
#define my_MPI_SIZE_T sc_MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
#define my_MPI_SIZE_T sc_MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
#define my_MPI_SIZE_T sc_MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
#define my_MPI_SIZE_T sc_MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
#define my_MPI_SIZE_T sc_MPI_UNSIGNED_LONG_LONG
#else
#define my_MPI_SIZE_T sc_MPI_UNSIGNED
#endif



p4est_dynres_data_t * p4est_dynres_data_new(const char *pset, p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack){
    int flag = 0;
    char boolean_string[10];
    char *key = "p4est_pset";
    char **barrier_pset;
    MPI_Info info;
    
    
    p4est_dynres_data_t *data = (p4est_dynres_data_t *) malloc(sizeof(p4est_dynres_data_t));
    data->session = MPI_SESSION_NULL;
    data->info = MPI_INFO_NULL;
    data->col = MPI_INFO_NULL;
    data->request = MPI_REQUEST_NULL;
    data->setop_pending = false;
    data->output_psets = NULL;
    data->noutput_psets = 0;
    data->pack = pack;
    data->unpack = unpack;
    int primary = 0;

    MPI_Session_init(MPI_INFO_NULL, MPI_ERRORS_RETURN, &data->session);
    /* Get the info from our mpi://WORLD pset */
    strcpy(data->main_pset, "mpi://WORLD");
    MPI_Session_get_pset_info (data->session, data->main_pset, &info);

    MPI_Info_get(info, "mpi_primary", 6, boolean_string, &flag);
    primary = 0;
    if(0 != strcmp(boolean_string, "True")){
        primary = 1;
    } 
    /* get value for the 'mpi_dyn' key -> if true, this process was added dynamically */
    MPI_Info_get(info, "mpi_dyn", 6, boolean_string, &flag);
    
    /* if mpi://WORLD is a dynamic PSet retrieve the name of the main PSet stored on mpi://WORLD */
    if(flag && 0 == strcmp(boolean_string, "True")){
        MPI_Info_free(&info);
        
        data->is_dynamic = true;
        data->setop_pending = true;
        MPI_Session_get_pset_data (data->session, data->main_pset, data->main_pset, &key, 1, true, &info);
        MPI_Info_get(info, key, MPI_MAX_PSET_NAME_LEN, data->main_pset, &flag);

        if(!flag){
            printf("No 'next_main_pset' was provided for dynamic process. Terminate.\n");
            p4est_dynres_data_free(&data);
            return NULL;
        }
        MPI_Info_free(&info);

        /* This process was spawned but the simulation is about to end so this process needs to terminate as well */
        /* Barrier PSet is transmitted as "p4est://canceled/[barrier_pset]"" */
        if(0 == strncmp(data->main_pset, "p4est://canceled/", 17)){
            barrier_pset = malloc(sizeof(char *));
            barrier_pset[0] = strdup(data->main_pset + 17);
            MPI_Session_pset_barrier(data->session, barrier_pset, 1, MPI_INFO_NULL);
            free_string_array(&barrier_pset, 1);
            p4est_dynres_data_free(&data);
            return NULL;
        }


    }else{
        data->is_dynamic = false;
        strcpy(data->main_pset, pset);
    }
    return data;
}

void p4est_dynres_data_free(p4est_dynres_data_t ** data_ptr){
    p4est_dynres_data_t *data = *data_ptr;
    if (MPI_INFO_NULL != data->info){
      MPI_Info_free(&data->info);
    }
    if (MPI_INFO_NULL != data->col){
      MPI_Info_free(&data->col);
    }
    if (MPI_REQUEST_NULL != data->request){
      MPI_Request_free(&data->request);
    }
    if (MPI_SESSION_NULL != data->session){
      MPI_Session_finalize(&data->session);
    }
    if(NULL != data->output_psets){
        free_string_array(&data->output_psets, data->noutput_psets);
    }
    free(data);
}

p4est_dynres_state_t * p4est_dynres_state_new(const char *pset, p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack, void * user_pointer){
    p4est_dynres_state_t * state = malloc(sizeof(p4est_dynres_state_t));
    state->p4est_dynres_data = (void *) p4est_dynres_data_new(pset, pack, unpack);

    if(NULL == state->p4est_dynres_data){
        p4est_dynres_state_free(&state);
        return NULL;
    }

    state->p4est = NULL;
    state->user_pointer = user_pointer;
    state->is_dynamic = ((p4est_dynres_data_t *)state->p4est_dynres_data)->is_dynamic;
    return state;
}
void p4est_dynres_state_free(p4est_dynres_state_t **state);
void p4est_dynres_state_free(p4est_dynres_state_t **state){
    if(NULL != (*state)->p4est_dynres_data){
        p4est_dynres_data_free((p4est_dynres_data_t **) &(*state)->p4est_dynres_data);
    }
    free(*state);
}

p4est_t            *
p4est_dynres_add_ext (p4est_t * p4est, sc_MPI_Comm new_mpicomm, 
                      int is_added, int num_added, 
                      void *user_pointer)
{

    int mpiret;

    //compute unknown input values
    if(is_added < 0)
        is_added = (p4est == NULL);

    if(num_added < 0) {
        mpiret = sc_MPI_Allreduce(&is_added, &num_added, 1, sc_MPI_INT, sc_MPI_SUM, new_mpicomm);
        SC_CHECK_MPI (mpiret);
    }

    if(num_added < 1)
        return p4est;


    //save old MPI environment
    sc_MPI_Comm oldcomm = sc_MPI_COMM_NULL;


    //p4est has to be created first
    if(p4est == NULL) {
        P4EST_ASSERT (is_added);

        //allocate memory and set parallel environment
        p4est = P4EST_ALLOC_ZERO (p4est_t, 1);
        p4est_comm_parallel_env_assign (p4est, new_mpicomm);
    } else {
        //save old parallel environment, assign new one
        oldcomm = p4est->mpicomm;
        p4est_dynres_comm_parallel_env_replace(p4est, new_mpicomm);
    }

    int num_procs = p4est->mpisize;

    //find rank of root process for following communication
    int *global_added = P4EST_ALLOC (int, num_procs);
    mpiret = sc_MPI_Allgather (&is_added, 1, sc_MPI_INT, 
            global_added, 1, sc_MPI_INT,
            new_mpicomm);
    SC_CHECK_MPI (mpiret);
    int root = -1;
    for(int i = 0; i < num_procs; i++) {
        if(!global_added[i]) {
            root = i;
            break;
        }
    }
    if(root < 0)
        P4EST_GLOBAL_LERROR ("p4est: p4est_process_add called with only new processes\n");


    p4est->user_pointer = user_pointer;


    //communicator containing all new processes and the root process
    sc_MPI_Comm new_procs_and_root;
    int color = (is_added || p4est->mpirank == root) ? 0 : MPI_UNDEFINED;
    int key = is_added ? 1 : 0;
    mpiret = sc_MPI_Comm_split(new_mpicomm, color, key, &new_procs_and_root);
    SC_CHECK_MPI (mpiret);

    if(new_procs_and_root != sc_MPI_COMM_NULL) {
        //broadcast the connectivity to new processes
        if(!is_added) {
            p4est_connectivity_bcast(p4est->connectivity, 0, new_procs_and_root);
        } else {
            p4est->connectivity = p4est_connectivity_bcast(NULL, 0, new_procs_and_root);
        }

        //broadcast quadrant data size
        mpiret = sc_MPI_Bcast(&(p4est->data_size), 1, my_MPI_SIZE_T, 0, new_procs_and_root);
        SC_CHECK_MPI (mpiret);

        mpiret = sc_MPI_Comm_disconnect((MPI_Comm *)  &new_procs_and_root);
        SC_CHECK_MPI (mpiret);
    }

    //init some member variables
    if(p4est->global_first_quadrant != NULL)
        P4EST_FREE(p4est->global_first_quadrant);
    p4est->global_first_quadrant = P4EST_ALLOC(p4est_gloidx_t, num_procs + 1);
    p4est_comm_count_quadrants(p4est);

    //these variables are already correct on the parent processes
    if(is_added) {
        //set tree indices and quadrant number to show
        //that this process is empty
        p4est->first_local_tree = -1;
        p4est->last_local_tree = -2;
        p4est->local_num_quadrants = 0;

        //allocate and initialize trees array
        if(p4est->trees != NULL)
            sc_array_destroy(p4est->trees);
        p4est->trees = sc_array_new (sizeof (p4est_tree_t));
        sc_array_resize (p4est->trees, p4est->connectivity->num_trees);
        for(int i = 0; i < p4est->connectivity->num_trees; i++){
            //init tree
            p4est_tree_t *tree = p4est_tree_array_index (p4est->trees, i);
            if(&tree->quadrants == NULL)
                sc_array_new (sizeof (p4est_quadrant_t));
            sc_array_init (&tree->quadrants, sizeof (p4est_quadrant_t));
            P4EST_QUADRANT_INIT(&(tree->first_desc));
            P4EST_QUADRANT_INIT(&(tree->last_desc));
            tree->quadrants_offset = 0;
            for(int j = 0; j <= P4EST_QMAXLEVEL; j++){
                tree->quadrants_per_level[j] = 0;
            }
            for(int j = P4EST_MAXLEVEL; j <= P4EST_MAXLEVEL; j++){
                tree->quadrants_per_level[j] = -1;
            }
            tree->maxlevel = 0;
        }

        //allocate memory pools
        if (p4est->user_data_pool != NULL)
            sc_mempool_destroy (p4est->user_data_pool);
        if (p4est->data_size > 0) {
            p4est->user_data_pool = sc_mempool_new (p4est->data_size);
        } else {
            p4est->user_data_pool = NULL;
        }
        p4est->quadrant_pool = sc_mempool_new (sizeof (p4est_quadrant_t));
    }

    //fill global partition information
    if(p4est->global_first_position != NULL)
        P4EST_FREE(p4est->global_first_position);
    p4est->global_first_position = P4EST_ALLOC_ZERO (p4est_quadrant_t, num_procs + 1);
    p4est_comm_global_partition (p4est, NULL);

    P4EST_FREE(global_added);
    if(p4est->mpicomm_owned && oldcomm != sc_MPI_COMM_NULL)
        MPI_Comm_disconnect((MPI_Comm *) &oldcomm);

    P4EST_ASSERT(p4est_is_valid(p4est));


    ////////////////////////////////////////////////////////////////
    //     DEBUG STUFF
    ////////////////////////////////////////////////////////////////

    return p4est;
}

p4est_t            *
p4est_dynres_add (p4est_t * p4est, sc_MPI_Comm new_mpicomm)
{
    return p4est_dynres_add_ext(p4est, new_mpicomm, -1, -1, NULL);
}

p4est_t            *
p4est_dynres_remove_ext (p4est_t * p4est, sc_MPI_Comm new_mpicomm, int is_removed, int num_removed, p4est_weight_t weight_fn)
{

    int mpiret;
    //compute unknown input values
    if(is_removed < 0)
        is_removed = (new_mpicomm == sc_MPI_COMM_NULL);

    if(num_removed < 0) {
        mpiret = sc_MPI_Allreduce(&is_removed, &num_removed, 1, sc_MPI_INT, sc_MPI_SUM, p4est->mpicomm);
        SC_CHECK_MPI (mpiret);
    }

    if(num_removed < 1)
        return p4est;

    if(weight_fn != NULL)
        P4EST_GLOBAL_LERROR ("p4est: Warning: Calling p4est_dynres_remove_ext with non-null weight function is not yet supproted. Using uniform partitioning instead.\n");

    sc_MPI_Comm oldcomm = p4est->mpicomm;
    int oldsize = p4est->mpisize;

    //collect information about the removed processes
    int *global_removed = P4EST_ALLOC (int, oldsize);
    mpiret = sc_MPI_Allgather (&is_removed, 1, sc_MPI_INT, 
            global_removed, 1, sc_MPI_INT,
            oldcomm);
    SC_CHECK_MPI (mpiret);
    int quadrants_moved = 0;
    for(int i = 0; i < oldsize; i++) {
        if(global_removed[i]) {
            quadrants_moved += p4est->global_first_quadrant[i+1]-p4est->global_first_quadrant[i];
        }
    }

    //calculate local target quadrant count
    p4est_locidx_t local_target;
    if(is_removed) {
        local_target = 0;
    } else {
        //distribute the affected quadrants equally between all processes

        int new_num_procs;
        mpiret = sc_MPI_Comm_size(new_mpicomm, &new_num_procs);
        SC_CHECK_MPI (mpiret);

        local_target = p4est->local_num_quadrants + (quadrants_moved / new_num_procs);
        
        int new_rank;
        mpiret = sc_MPI_Comm_rank(new_mpicomm, &new_rank);
        SC_CHECK_MPI (mpiret);
        if(new_rank < quadrants_moved % new_num_procs)
            local_target++;
    }

    //communicate distribution array
    int num_procs = p4est->mpisize;
    int *target_distribution = P4EST_ALLOC (p4est_locidx_t, num_procs);
    mpiret = sc_MPI_Allgather (&local_target, 1, P4EST_MPI_LOCIDX, 
            target_distribution, 1, P4EST_MPI_LOCIDX,
            p4est->mpicomm);
    SC_CHECK_MPI (mpiret);


    //repartition according to the target quadrant counts
    int global_shipped = p4est_partition_given (p4est, target_distribution);
    if (global_shipped) {
        /* the partition of the forest has changed somewhere */
        ++p4est->revision;
    }
    //now, exactly the removed processes should be empty
    //remove all empty processes
    int p4est_exists = p4est_dynres_comm_parallel_env_reduce(&p4est);
    P4EST_FREE (global_removed);
    P4EST_FREE (target_distribution);
    
    if(p4est_exists) {
        P4EST_ASSERT(p4est_is_valid(p4est));
        return p4est;
    } else {
        return NULL;
    }
}


p4est_t            *
p4est_dynres_remove (p4est_t * p4est, sc_MPI_Comm new_mpicomm)
{
    return p4est_dynres_remove_ext (p4est, new_mpicomm, -1, -1, NULL);
}


p4est_t            *
p4est_dynres_replace_ext (p4est_t *p4est, sc_MPI_Comm new_mpicomm, 
        int is_added, int num_added,
        int is_removed, int num_removed, 
        void *user_pointer, p4est_weight_t weight_fn) 
{
    int mpiret;

    //compute unknown input values
    if(is_removed < 0)
        is_removed = (new_mpicomm == sc_MPI_COMM_NULL);

    if(is_added < 0)
        is_added = (p4est == NULL);


    //This group contains all processes in the final communicator
    sc_MPI_Group new_group = sc_MPI_GROUP_EMPTY;
    if(!is_removed) {
        mpiret = sc_MPI_Comm_group(new_mpicomm, &new_group);
        SC_CHECK_MPI (mpiret);
    }
    
    //This group contains all processes from the old communicator
    sc_MPI_Group old_group = sc_MPI_GROUP_EMPTY;
    sc_MPI_Comm old_comm;
    if(!is_added) {
        old_comm = p4est->mpicomm;
        mpiret = sc_MPI_Comm_group(old_comm, &old_group);
        SC_CHECK_MPI (mpiret);
    }

    //Intersection between the previous groups, contains all processes
    //kept over this operation
    sc_MPI_Group kept_group;
    mpiret = sc_MPI_Group_intersection(old_group, new_group, &kept_group);
    SC_CHECK_MPI (mpiret);
    int is_kept = kept_group != sc_MPI_GROUP_EMPTY;

    //for the old processes, create the intersection communicator
    //and remove processes outside it
    sc_MPI_Comm kept_comm;
    if(num_removed != 0 && !is_added) {
        mpiret = sc_MPI_Comm_create(old_comm, kept_group, &kept_comm);
        SC_CHECK_MPI (mpiret);

        //remove processes
        if(num_removed > 0) {
            p4est = p4est_dynres_remove_ext(p4est, kept_comm, is_removed, num_removed, weight_fn);
        } else {
            p4est = p4est_dynres_remove_ext(p4est, kept_comm, -1, -1, weight_fn);
        }
    }

    //now, the removed processes are no more of interest.
    //Add the new ones
    if(num_added != 0 && !is_removed) {
        if(num_added > 0) {
            p4est = p4est_dynres_add_ext(p4est, new_mpicomm, is_added, num_added, user_pointer);
        } else {
            p4est = p4est_dynres_add_ext(p4est, new_mpicomm, -1, -1, user_pointer);
        }
    }

    if(!is_removed)
        sc_MPI_Group_free(&new_group);
    if(!is_added)
        sc_MPI_Group_free(&old_group);
    if(is_kept)
        MPI_Comm_disconnect((MPI_Comm *) &kept_comm);
    sc_MPI_Group_free(&kept_group);


    if(p4est != NULL)
        P4EST_ASSERT(p4est_is_valid(p4est));

    return p4est;
}

p4est_t            *
p4est_dynres_replace (p4est_t *p4est, sc_MPI_Comm new_mpicomm)
{
   return p4est_dynres_replace_ext(p4est, new_mpicomm, -1, -1, -1, -1, NULL, NULL);
}
/**
 * @brief      Serialize and Send an MPI Info object
 *
 * @details    Uses the info_serialized struct to fill a byte buffer with
 * strings and offsets which is then sent via mpi
 *
 * @param      info The info object to send
 *
 * @param      dest The rank of the recipient in the communicator
 *
 * @param      tag1 The MPI tag used for the first MPI_Send operation (should
 * match the tag1 argument in the MPIDYNRES_Recv_MPI_Info function)
 *
 * @param      tag2 The MPI tag used for the second MPI_Send operation (should
 * match the tag2 argument in the MPIDYNRES_Recv_MPI_Info function)
 *
 * @param      comm The communicator used
 *
 * @return     On error, a value != 0 is returned
 */
int p4est_dynres_send_info(sc_MPI_Comm comm, MPI_Info info) {
    int res;
    int nkeys;
    int vlen;
    int unused;
    int rank;
    char key[MPI_MAX_INFO_KEY + 1] = {0};
    struct info_serialized *serialized;

    sc_MPI_Comm_rank(comm, &rank);

    if (info == MPI_INFO_NULL) {
        size_t bufsize = SIZE_MAX;
        //res = MPI_Send(&bufsize, 1, my_MPI_SIZE_T, dest, tag1, comm);
        res = sc_MPI_Bcast(&bufsize, 1, my_MPI_SIZE_T, 0, comm);
        if (res) {
          return res;
        }
        return 0;
    }

    res = MPI_Info_get_nkeys(info, &nkeys);
    if (res) {
      return res;
    }
    size_t bufsize = sizeof(struct info_serialized) + 2 * nkeys * sizeof(ptrdiff_t);

    for (int i = 0; i < nkeys; i++) {
      MPI_Info_get_nthkey(info, i, key);
      MPI_Info_get_valuelen(info, key, &vlen, &unused);
      bufsize += sizeof(char) * (strlen(key) + 1);
      bufsize += sizeof(char) * (vlen + 1);
    }

    uint8_t *buffer = calloc(1, bufsize);

    if (!buffer) {
      printf("Memory error!\n");
      exit(EXIT_FAILURE);
    }

    if(rank == 0){
        serialized = (struct info_serialized *)buffer;
        serialized->num_strings = 2 * (size_t)nkeys;

        size_t offset =
            sizeof(struct info_serialized) + 2 * nkeys * sizeof(ptrdiff_t);

        for (int i = 0; i < nkeys; i++) {
          uint8_t *keystr = buffer + offset;
          serialized->strings[2 * i] = keystr - buffer;
          MPI_Info_get_nthkey(info, i, key);
          strcpy((char *)keystr, key);
          offset += sizeof(char) * (strlen(key) + 1);
          uint8_t *valstr = buffer + offset;
          serialized->strings[2 * i + 1] = valstr - buffer;
          MPI_Info_get_valuelen(info, (char *)keystr, &vlen, &unused);
          MPI_Info_get(info, (char *)keystr, vlen, (char *)valstr, &unused);
          offset += sizeof(char) * (vlen + 1);
        }
    }
    
    res = sc_MPI_Bcast(&bufsize, 1, my_MPI_SIZE_T, 0, comm);
    if (res) {
      free(buffer);
      return res;
    }

    res = sc_MPI_Bcast(buffer, bufsize, sc_MPI_BYTE, 0, comm);
    free(buffer);
    if (res) {
      return res;
    }
    return 0;
}


/**
 * @brief      Receive and deserialize an MPI Info object
 *
 * @details    Receives an info_serialized struct via MPI and deserializes it
 *
 * @param      info The info object that will be returned
 *
 * @param      source The senders rank in the communicator or MPI_ANY_SOURCE
 *
 * @param      tag1 The MPI tag used for the first MPI_Recv operation (should
 * match the tag1 argument in the MPIDYNRES_Send_MPI_Info function)
 *
 * @param      tag2 The MPI tag used for the second MPI_Recv operation (should
 * match the tag2 argument in the MPIDYNRES_Send_MPI_Info function)
 *
 * @param      comm The communicator used
 *
 * @return     On error, a value != 0 is returned
 */
int p4est_dynres_recv_info(sc_MPI_Comm comm, MPI_Info *info) {
    int res;
    size_t bufsize;
    struct info_serialized *serialized;

    //res = MPI_Recv(&bufsize, 1, my_MPI_SIZE_T, source, tag1, comm, status1);
    res = sc_MPI_Bcast(&bufsize, 1, my_MPI_SIZE_T, 0, comm);
    if (res) {
      return res;
    }

    if (bufsize == SIZE_MAX) {
      *info = MPI_INFO_NULL;
      return 0;
    }
    uint8_t *buf = calloc(bufsize, 1);
    if (!buf) {
      printf("Memory error!\n");
      exit(EXIT_FAILURE);
    }
    //res = MPI_Recv(buf, bufsize, MPI_BYTE, source, tag2, comm, status2);
    res = sc_MPI_Bcast(buf, bufsize, sc_MPI_BYTE, 0, comm);
    if (res) {
      free(buf);
      return res;
    }
    serialized = (struct info_serialized *)buf;
    res = MPI_Info_create(info);
    if (res) {
      free(buf);
      return res;
    }
    for (size_t i = 0; i < serialized->num_strings / 2; i++) {
      char *key = (char *)(buf + serialized->strings[2 * i]);
      char *val = (char *)(buf + serialized->strings[2 * i + 1]);
      res = MPI_Info_set(*info, key, val);
      if (res) {
        free(buf);
        return res;
      }
    }
    free(buf);
    return 0;
}



void free_string_array(char ***array, int size){
    int i;
    if(0 == size){
        *array = NULL;
        return;
    }
    for(i = 0; i < size; i++){
        free((*array)[i]);
    }
    
    free(*array);

    *array = NULL;
}
