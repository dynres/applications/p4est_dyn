#ifndef P4EST_DYNRES_INTERNAL_H
#define P4EST_DYNRES_INTERNAL_H

#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

#include "mpi.h"
#include "p4est.h"
#include "p4est_dynres_common.h"

#define sc_MPI_Comm_disconnect MPI_Comm_disconnect

typedef struct p4est_dynres_data{
  char delta_add[MPI_MAX_PSET_NAME_LEN];
  char delta_sub[MPI_MAX_PSET_NAME_LEN];
  char main_pset[MPI_MAX_PSET_NAME_LEN];
  MPI_Session session;
  MPI_Info info;
  MPI_Info col;
  MPI_Request request;
  char **output_psets;
  int noutput_psets;
  int setop;
  int rank;
  bool setop_pending;
  bool is_dynamic;
  p4est_dynres_pack_func_t pack;
  p4est_dynres_pack_func_t unpack;
}p4est_dynres_data_t;

p4est_dynres_data_t * p4est_dynres_data_new(const char *pset, p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack);
void p4est_dynres_data_free(p4est_dynres_data_t ** data_ptr);

p4est_dynres_state_t * p4est_dynres_state_new(const char *pset, p4est_dynres_pack_func_t pack, p4est_dynres_unpack_func_t unpack, void *user_pointer);
void p4est_dynres_state_free(p4est_dynres_state_t **state);


/*
 * Serialization of an MPI_Info object
 */
typedef struct info_serialized{
  size_t num_strings;   // number of strings (has to be multiple of 2)
  ptrdiff_t strings[];  // difference from the base of this struct
} info_serialized_t;


p4est_t * p4est_dynres_replace_ext (p4est_t *p4est, sc_MPI_Comm new_mpicomm, 
        int is_added, int num_added,
        int is_removed, int num_removed, 
        void *user_pointer, p4est_weight_t weight_fn);

void free_string_array(char ***array, int size);

int p4est_dynres_recv_info(sc_MPI_Comm comm, MPI_Info *info);
int p4est_dynres_send_info(sc_MPI_Comm comm, MPI_Info info);


#endif /* !P4EST_DYNRES_INTERNAL_H */